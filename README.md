

# Configuration d'un bon Sublime text 3




## 1 - Visuel

* <h3>Outils de personnalisation</h3>
	* ColorSublime	 : Recherche et instalation de thème
	* Themr          : Personnalisation et selection du thème
	* Schemr         : Personnalisation et selection du schem
	* Meterial Theme : Configuration et option de thème + icon side bar
	* A File icon		 : Fourni les icon de side bar pour Material Theme

* <h3>Mes Thèmes et Schems utilisé</h3>
	* Thème - Adaptive
	* Thème - Arzy
	* Schem - Adventure_Time [Dark]
	* Schem - Heliopolis [Dark]



## 2 - Code

* <h3>Productivité</h3>
	* Alignment         : Permet l'alignement de plusieur lignes à partir d'un point de référence
	* CodeFormatter     : Permet de formatter le code pour un rendu plus propre
	* SublimeCodeIntel  : Offre une auto-complétion semblabe à celle d'un IDE en allant chercher les class et methodes du projet en cours
	* PHPIntel (etc..)  : Extention optionnel de SublimeCodeIntel
		* Utilisation avancé, inutil la plupart du temps. Créé un répertoire dans le projet donc attention à ne pas utiliser si inutil
		 et à rajouter dans le gitignore les répertoires ".phpintel"
	* AutoFileName      : Auto-complétion pour les path dans votre code. Pratique quand on à du mal à si retrouver dans la structure du projet
	* DocBlockr				  : Facilite l'écriture de la doc de votre code PHP pour fournir l'auto-complétion de vos functions
	* Color Picker		  : Un color picker intégré à Sublime qui vous sera bien utile pour tout ce qui est CSS
	* Color Highlighter : Affiche la couleur que représente une valeur hexadecimal ou alphabétique dans votre code. Très pratique en CSS.
	* Console Wrap :

* <h3>Analyse du code</h3>
	* BracketHighlighter : Focus la section du code ou l'on se trouve par rapport aux acolades entre lesquelles on se situe
	* GitGutter          : Vous montre quel portion du code sont à jour avec git. Permet de comparer avec un ancien commit. Et plein d'autre choses !!
	* SublimeLinter      : Ajouter l'extention du langage souhaiter pour avoir un linter directement intégré à votre Sublime



## 3 - Outils

* <h3>Organisation</h3>
	* Project Manager : Permet de gérer tout vos projet avec Sublime.
		* Creation, Edition
		* Ce qui apparait ou non dans l'arbo de la side bar
		* Préférence système spécific au projet. (synthax par defaut, thème et schem, taille de police, parametrage des packages, etc..)
		* Build système spécific au projet
	* PlainTasks      : Un editeur de TODO. Plus besoin de s'embéter avec Trello et compagnie il suffit d'ouvrir sa Todo dans Sublime

* <h3>Documentation</h3>
	* DevDocs : Vous donne accès à l'API de DevDocs. C'est une documentation communautaire qui regroupe de très nombreuse techno.
	Très pratique pour comprendre comment utiliser tel ou tel fonction sans devoir chercher 3h dans des docs officiel parfois très mal foutu !

* <h3>Embarqué</h3>
	* SQLTools  : Vous permet d'écrire et de jouer vos requètes SQL directement dans Sublime. Prend en charge les DB locals et distante.
	Très pratique, fais gagné du temps et permet d'obtenir une bon maitrise des langage de requetage.
	* Requester : Même principe que SQLTools mais pour les API. Plus de besoin de switcher avec postman.
	Avec un peu d'imagination ça devient un outil extrement puissant et offre un gain de temps non négligeable.


## 4 - Sublime Project

Sublime Text embarque une gestion de projet assez sympas.
Le package Project Manager vous donne facilement la main dessus.
Vous trouverez dans le repo un fichier type de configuration du projet, vous montrant les possibles custumisations
