<?php

$service_prefix = $argv[1];
$separator      = $argv[2];
$path           = $argv[3];
$counter        = 0;

$path_subl_file = "/home/carpen_g/.config/sublime-text-3/Packages/User/My_Completions/symfony_service.sublime-completions";
$json           = json_decode(file_get_contents($path_subl_file));

$json->completions = array();

$command_service = "php $path/bin/console debug:container --show-private | grep ' $service_prefix\\$separator'";
$output_service  = array();

exec($command_service, $output_service);
sleep(1);

print_r($output_service);
$final_list_service = array();

foreach ($output_service as $key => $service) {

    $service_info = explode(' ', $service);
    $service      = array();
    $bool         = false;

    foreach ($service_info as $info) {
        if ($info != '') {
            $explode_info = explode($separator, $info);
            $info_prefix  = $explode_info[0];
            if ($info_prefix == $service_prefix) {
                $service[0] = $info;
                $bool       = true;
            } elseif ($bool) {
                $service[1] = $info;
            }
        }
    }

    if (count($service) > 0) {
        $json->completions[$counter]["trigger"]  = $service[0] . "\tsymfony";
        $json->completions[$counter]["contents"] = "'$service[0]'";
        $counter++;
    }

}

$json_str = json_encode($json);
$json_arr = str_split($json_str);

$final_json = '';
$count      = 0;

foreach ($json_arr as $key => $value) {

    if ($value == '{' || $value == '[') {

        $count++;

        $final_json .= $value;
        $final_json .= "\n";

        for ($i = 0; $i < $count; $i++) {
            $final_json .= "\t";
        }
    } elseif ($value == '}' || $value == ']') {
        $count--;
        $final_json .= "\n";

        for ($i = 0; $i < $count; $i++) {
            $final_json .= "\t";
        }

        $final_json .= $value;
    } elseif ($value == ',') {
        $final_json .= $value;
        $final_json .= "\n";

        for ($i = 0; $i < $count; $i++) {
            $final_json .= "\t";
        }

    } else {
        $final_json .= $value;
    }
}

file_put_contents($path_subl_file, $final_json);
